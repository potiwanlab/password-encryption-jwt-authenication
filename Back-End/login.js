const Hapi = require('@hapi/hapi');
const { payload } = require('@hapi/hapi/lib/validation');
const Jwt = require('@hapi/jwt');
const jwt = require('jsonwebtoken');

user = {
    name: "myname",
    surname: "mysurname",
    email: "test@test.com"
}

const init = async () => {
    const server = new Hapi.Server({
        port: 3001,
        host: 'localhost'
    });
    await server.register(Jwt);
    server.auth.strategy('my_jwt_stategy', 'jwt', {
        keys: 'some_shared_secret',
        verify: {
            aud: 'urn:audience:test',
            iss: 'urn:issuer:test',
            sub: false,
            nbf: true,
            exp: true,
            maxAgeSec: 14400,
            timeSkewSec: 15
        },
        validate: (artifacts, request, h) => {
            if (artifacts.decoded.payload.payload.name === user.name
                && artifacts.decoded.payload.payload.surname === user.surname
                && artifacts.decoded.payload.payload.email === user.email)
                return {
                    isValid: true,
                    credentials: {
                        name: artifacts.decoded.payload.payload.name,
                        surname: artifacts.decoded.payload.payload.surname,
                        email: artifacts.decoded.payload.payload.email
                    }
                };
            else
                return {
                    isValid: false
                }
        }
    });
    server.route({
        method: 'PUT',
        path: '/gettoken',
        config: {
            cors: true,
            handler(request, h) {
                const payload = request.payload;
                const token = jwt.sign({
                    payload,
                    aud: 'urn:audience:test',
                    iss: 'urn:issuer:test',
                    sub: false,
                    maxAgeSec: 14400,
                    timeSkewSec: 15
                }, 'some_shared_secret');
                var data = {
                    token: token
                }
                return data;
            },
        }
    });
    server.route({
        method: 'GET',
        path: '/login',
        config: {
            cors: true,
            handler(request, h) {
                var data = {
                    message: `Welcome back ${request.auth.credentials.name}`
                }
                return data;
            },
            auth: {
                strategy: 'my_jwt_stategy',
            }
        }
    });
    await server.start();
    console.log('Server running at:', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});
init();
