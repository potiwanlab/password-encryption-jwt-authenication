import React, { useState } from 'react';
import './App.css';

function App() {

  const [submitLink, setlink] = useState("http://localhost:3001/gettoken");
  const [page, setPage] = useState("gettoken")
  const [name, setName] = useState("myname");
  const [surname, setSurname] = useState("mysurname");
  const [email, setEmail] = useState("test@test.com");
  const [token, setToken] = useState("")
  const [message, setMessage] = useState("")

  return (
    <div className="App">
      <header className="App-header">
        <button onClick={() => setlinkFunction("http://localhost:3001/gettoken", "gettoken")}>Get Token</button>
        <button onClick={() => setlinkFunction("http://localhost:3001/login", "login")}>Token Login</button>
        {
          page === "gettoken" ? (
            <form>
              <label htmlFor="name">First name:</label><br></br>
              <input type="text" id="name" name="name" value={name} onChange={(e) => onChange(e, setName)} /><br></br>
              <label htmlFor="surname">Last name:</label><br></br>
              <input type="text" id="surname" name="surname" value={surname} onChange={(e) => onChange(e, setSurname)}></input><br></br>
              <label htmlFor="email">Email:</label><br></br>
              <input type="text" id="email" name="email" value={email} onChange={(e) => onChange(e, setEmail)}></input><br></br>
              <input type="submit" value="Submit" onClick={(e) => submitForm(e)}></input>
            </form>
          ) : (
            <form>
              <label htmlFor="token">Token:</label><br></br>
              <input type="text" id="token" name="token" value={token} onChange={(e) => onChange(e, setToken)} /><br></br>
              <input type="submit" value="Submit" onClick={(e) => submitFormToken(e)}></input>
            </form>
          )
        }
        {
          token !== "" && page !== "login" ? (
            <div style={{ width: "80%", wordWrap: "break-word" }}>Your token is {token}</div>
          ) : (false)
        }
        {
          message !== "" && page !== "gettoken" ? (
            <div style={{ width: "80%", wordWrap: "break-word" }}>{message}</div>
          ) : (false)
        }
      </header>
    </div>
  );

  function submitForm(e) {
    e.preventDefault();
    fetch(submitLink, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: name,
        surname: surname,
        email: email
      })
    }).then(response => response.json())
      .then(data => setToken(data.token));
  }

  function submitFormToken(e) {
    e.preventDefault();
    fetch(submitLink, {
      method: 'GET',
      headers: { "Authorization": `Bearer ${token}` }
    }).then(response => response.json())
      .then(data => setMessage(data.message));
  }

  function onChange(e, setFunction) {
    setFunction(e.target.value)
  }

  function setlinkFunction(link, page) {
    setlink(link)
    setPage(page)
  }

}

export default App;
